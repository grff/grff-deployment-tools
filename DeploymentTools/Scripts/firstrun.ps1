#########################################################################################################
##First run PowerShell Script to load deployment tools (scripts, branding files, etc) and CNGEN binary.##
#########################################################################################################

#Delete any leftover packer scheduled tasks
Write-Host 'Deleting any leftover Packer scheduled tasks' -Foregroundcolor Yellow -Backgroundcolor Black
$pattern = "packer"
SCHTASKS /QUERY /FO CSV | ConvertFrom-CSV | Where-Object {$_.TaskName -match $pattern} | Foreach  {SCHTASKS /DELETE /TN $_.taskname /F}

#Delete any OneDrive Tasks
Write-Host 'Deleting any OneDrive scheduled tasks' -Foregroundcolor Yellow -Backgroundcolor Black
$pattern = "OneDrive"
SCHTASKS /QUERY /FO CSV | ConvertFrom-CSV | Where-Object {$_.TaskName -match $pattern} | Foreach  {SCHTASKS /DELETE /TN $_.taskname /F}

#Set PS execution policy and disable UAC
Write-Host 'Setting PS execution policy and disabling UAC' -Foregroundcolor Yellow -Backgroundcolor Black
New-ItemProperty -Path HKLM:Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -PropertyType DWord -Value 0 -Force

#Install .NET 3.5
Write-Host 'Installing .NET 3.5' -Foregroundcolor Yellow -Backgroundcolor Black
DISM.EXE /Online /Add-Package /PackagePath:C:\grffTools\DeploymentTools\Binaries\microsoft-windows-netfx3-ondemand-package.cab
Start-Sleep 3

#Wallpaper Function
Function Set-WallPaper($Value)
	{
	 Set-ItemProperty -path 'HKCU:\Control Panel\Desktop\' -name wallpaper -value $value
	 rundll32.exe user32.dll, UpdatePerUserSystemParameters
	}

#Set Default Wallpaper
Write-Host 'Setting wallpaper' -Foregroundcolor Yellow -Backgroundcolor Black
Set-WallPaper -value "C:\grffTools\DeploymentTools\slides\vcu-grey.png"
Start-Sleep 2

#Set Theme
Write-Host 'Setting theme' -Foregroundcolor Yellow -Backgroundcolor Black
Copy-Item "C:\grffTools\DeploymentTools\Branding\grff.theme" "C:\Windows\resources\Themes" -Force
Start-Process "C:\grffTools\DeploymentTools\Branding\grff.deskthemepack" 
Start-Sleep 3
(New-Object -comObject Shell.Application).Windows() | where-object {$_.LocationName -eq "Personalization"} | foreach-object {$_.quit()}
Set-ItemProperty -Path "Registry::HKEY_CURRENT_USER\Software\Microsoft\Windows\DWM" -Name ColorPrevalence -Value 1 -Type DWord -Force
Start-Sleep 3
taskkill /F /IM explorer.exe
taskkill /F /IM systemsettings.exe
Start-Process explorer.exe
Start-Sleep 3
taskkill /F /IM explorer.exe

#KMS Activate Windows
#Write-Host 'KMS Activating VM' -Foregroundcolor Yellow -Backgroundcolor Black
#cmd /c "C:\grffTools\DeploymentTools\Scripts\VCUKMSACT.CMD"

#Install Chocolatey
Write-Host 'Installing Chocolatey' -Foregroundcolor Yellow -Backgroundcolor Black
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#Load DefaultUser NTUSER.dat registry hive as PSDrive and import registry files
Write-Host 'Loading Default User Profile Registry Hive and importing values' -Foregroundcolor Yellow -Backgroundcolor Black
Write-Host "Attempting to mount default user registry hive"
REG LOAD HKLM\Default C:\Users\Default\NTUSER.DAT
Write-Host "Writing values to default user registry hive"
Start-Sleep 3
#Default Background Color
Set-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Default\Control Panel\Colors" -Name Background -Value "99 101 106" -Type String -Force
New-Item -Path "Registry::HKEY_LOCAL_MACHINE\Default\Software\Microsoft\Windows\CurrentVersion\" -Name ImmersiveShell -Force
Start-Sleep 3
New-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Default\Software\Microsoft\Windows\CurrentVersion\ImmersiveShell" -Name SignInMode -Value 1 -PropertyType DWord -Force
New-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Default\Software\Microsoft\Windows\CurrentVersion\ImmersiveShell" -Name TabletMode -Value 0 -PropertyType DWord -Force
Start-Sleep 3
Remove-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Default\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" -Name OneDriveSetup -Force

#Dismount DefaultUser registry hive
Write-Host 'Dismount Default User Profile Registry' -Foregroundcolor Yellow -Backgroundcolor Black
$unloaded = $false
$attempts = 0
while (!$unloaded -and ($attempts -le 5)) {
  [gc]::Collect() # necessary call to be able to unload registry hive
  REG UNLOAD HKLM\Default
  $unloaded = $?
  $attempts += 1
}
if (!$unloaded) {
  Write-Warning "Unable to dismount default user registry hive at HKLM\DEFAULT - manual dismount required"
}

#Turn-Off Hibernate
Write-Host 'Turning off hibernate' -Foregroundcolor Yellow -Backgroundcolor Black
powercfg.exe /hibernate off

#Run base installs
Write-Host 'Running base installs' -Foregroundcolor Yellow -Backgroundcolor Black
$source 		= "https://bitbucket.org/grff/base-installs/raw/3745c16e18bd41f51dade50bf67f67eaf6094372/grff-base-Win10.txt"
$destination	= "C:\grffTools\DeploymentTools\Scripts\grff-base-win10.ps1"
Invoke-WebRequest $source -Outfile $destination
Invoke-Expression "C:\grffTools\DeploymentTools\Scripts\grff-base-win10.ps1"

#Install Web Browser Local Preferences
Write-Host 'Updating web browser local preferences' -Foregroundcolor Yellow -Backgroundcolor Black
Invoke-Expression "C:\grffTools\DeploymentTools\Scripts\webcntrl\webcntrl.cmd"

#Fix VLC shortcut
Write-Host 'Fixing VLC shortcut' -Foregroundcolor Yellow -Backgroundcolor Black
$AppLocation = "C:\Program Files\VideoLAN\VLC\vlc.exe"
$WshShell = New-Object -ComObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\VideoLAN\VLC Media Player.lnk")
$Shortcut.TargetPath = $AppLocation
$Shortcut.Arguments ="--no-qt-privacy-ask --no-qt-updates-notif"
$Shortcut.IconLocation = $AppLocation
$Shortcut.Description ="VLC Media Player"
$Shortcut.WorkingDirectory ="C:\Program Files\VideoLAN\VLC"
$Shortcut.Save()

#Debloat and remove all AppX packages Add back calculator, camera, and paint apps
Write-Host 'Debloating image' -Foregroundcolor Yellow -Backgroundcolor Black
Remove-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run" -Name SunJavaUpdateSched -Force
Remove-ItemProperty -Path "Registry::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run" -Name ConEmuDefaultTerminal -Force
Remove-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run" -Name ConnectionCenter -Force
Remove-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run" -Name Redirector -Force
Remove-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run" -Name Persistence -Force
Remove-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run" -Name IgfxTray -Force
Remove-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run" -Name SecurityHealth -Force
Set-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\SessionData\1" -Name LoggedOnSAMUser -Value "" -Type String -Force
Set-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\SessionData\1" -Name LoggedOnUser -Value "" -Type String -Force
del C:\Users\Public\Desktop\*.lnk
SchTasks.exe /Delete /TN "klcp_update" /F
SchTasks.exe /Delete /TN "Adobe Acrobat Update Task" /F
SchTasks.exe /Delete /TN "Adobe Flash Player Updater" /F
SchTasks.exe /Delete /TN "GoogleUpdateTaskMachineCore" /F
SchTasks.exe /Delete /TN "GoogleUpdateTaskMachineUA" /F
Invoke-Expression "C:\grffTools\DeploymentTools\Scripts\RemoveAppXs.ps1"
Invoke-Expression "C:\grffTools\DeploymentTools\Scripts\debloat\scripts\remove-default-apps.ps1"
Invoke-Expression "C:\grffTools\DeploymentTools\Scripts\debloat\scripts\remove-onedrive.ps1"
Start-Sleep 3
taskkill /F /IM explorer.exe
Start-Sleep 3

#Add back calculator, camera, and paint apps
Write-Host 'Adding back calculator, camera, and paint apps' -Foregroundcolor Yellow -Backgroundcolor Black
Invoke-Expression "C:\grffTools\DeploymentTools\Scripts\InstallAppXs.ps1"

Start-Sleep 3

$scriptPath   = 'C:\grffTools\DeploymentTools\Scripts\display-countdown.ps1'
$argumentList = '-Minutes .99 -Message "Initial image provisioning has completed" -Popup'
Invoke-Expression "& `"$scriptPath`" $argumentList"
shutdown -r /t 00


