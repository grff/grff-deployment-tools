# Set the name of the File
$AppFile = [System.IO.Path]::GetTempFileName()

# Build string containing all appx packages found
$Appx = Get-AppxPackage | select name

# create txt file of appx packages without the header
$appx | ft -hide | Out-File -FilePath $AppFile | Out-Null

# removes blank spaces from end of each line
$content = Get-Content $AppFile
$content | Foreach {$_.TrimEnd()} | Set-Content $AppFile

# removes the last blank line at the bottom
$content = [System.IO.File]::ReadAllText("$AppFile")
$content = $content.Trim()
[System.IO.File]::WriteAllText("$AppFile", $content)

# Uninstall each appx package from the clean list
Get-Content $AppFile | Foreach-Object {get-appxpackage -allusers $_  | remove-appxpackage}

# Remove appx file
Remove-Item $AppFile
