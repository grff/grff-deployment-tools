@echo off

rem tell chrome he's king and other things
copy "%~dp0master_preferences"  "%ProgramFiles%\Google\Chrome\Application"

rem copy files to control Firefox
copy "%~dp0autoconfig.js" "%ProgramFiles%\Mozilla Firefox\defaults\pref"
copy "%~dp0local-settings.js" "%ProgramFiles%\Mozilla Firefox\defaults\pref"
copy "%~dp0mozilla.cfg" "%ProgramFiles%\Mozilla Firefox"
copy "%~dp0override.ini" "%ProgramFiles%\Mozilla Firefox\browser"

rem defeat IE in the registry
reg add "HKCU\Software\Microsoft\Internet Explorer\Main" /f /v "Start Page" /d "http://go.vcu.edu/classroomsupport"
reg add "HKCU\Software\Microsoft\Internet Explorer\Main" /f /v "Check_Associations" /d "no" /t REG_SZ