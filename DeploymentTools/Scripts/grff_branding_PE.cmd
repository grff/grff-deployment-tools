@echo off
Echo.
Echo. This should be run is PE - not a windows session
Echo.
pause
Echo.
del C:\Windows\Web\Screen\*.png
copy "%~dp0grff.Theme" "C:\Windows\Resources\Themes"
copy "%~dp0\Web\Wallpaper\Windows\*.*" "C:\Windows\Web\Wallpaper\Windows\"
copy "%~dp0\User\*.*" "C:\ProgramData\Microsoft\User Account Pictures"
copy "%~dp0\Web\Screen\*.*" "C:\Windows\Web\Screen"