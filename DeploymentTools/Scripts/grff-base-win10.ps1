function production {

	#Academic Licensed Software
	Write-Host '#Licensed Software#' -Foregroundcolor Yellow -Backgroundcolor Black
	
	Write-Host "Installing Microsoft Office"
	choco install vcuOfficeProPlus -s http://nuget.ts.vcu.edu/nuget/ASL/ -y 

	#General Utility Software
	Write-Host '#General Utility Software#' -Foregroundcolor Yellow -Backgroundcolor Black
	
	Write-Host "Installing Firefox ESR"
	choco install FirefoxESR -y

	Write-Host "Installing JRE"
	choco install javaruntime -y

	Write-Host "Installing Adobe Air"
	choco install adobeair -y

	Write-Host "Installing Adobe Flash Firefox"
	choco install flashplayerplugin --y

	Write-Host "Installing Adobe Flash Opera/Chrome"
	choco install flashplayerppapi -y

	Write-Host "Installing Adobe Shockwave"
	choco install adobeshockwaveplayer -y

	Write-Host "Installing Silverlight"
	choco install Silverlight -y


}


function alldesktops {

	Write-Host "Installing Google Chrome"
	choco install GoogleChrome -y

	Write-Host "Installing 7-zip"
	choco install 7zip.install -y

	Write-Host "Installing Adobe Acrobat Reader DC"
	choco install adobereader -y

	Write-Host "Installing CCCP"
	choco install cccp -y

	Write-Host "Installing K-Lite"
	choco install k-litecodecpackmega -y

	Write-Host "Installing VLC"
	choco install vlc -y

	Write-Host "Installing NetTime"
	choco install nettime -y
	
	Write-Host "Installing CCleaner (temporary usage)"
	choco install ccleaner -y
	
	Write-Host "Installing CCEnhancer"
	choco install ccenhancer -y
	
	Write-Host "Installing 1password"
	choco install 1password -y

	Write-Host "Installing 1password-desktoplauncher"
	choco install 1password-desktoplauncher -y
	
	
	

}

function operations {
	
	Write-Host "Installing RSAT tools"
	choco install RSAT -y
	
	Write-Host "Installing Vivaldi"
	choco install vivaldi -y
	
	Write-Host "Installing Vagrant"
	choco install vagrant -y
	
	Write-Host "Install Packer"
	choco install packer -y
	
	Write-Host "Installing Wireshark"
	choco install wireshark -y
	
	Write-Host "Installing Notepad++"
	choco install notepadplusplus -y
	
	Write-Host "Installing SourceTree"
	choco install sourcetree -y
	
	Write-Host "Installing Cmder"
	choco install cmder -y
	
	Write-Host "Installing WinSCP"
	choco install winscp -y
	
	Write-Host "Installing GreenShot"
	choco install greenshot -y
	
	Write-Host "Installing Node.js"
	choco install nodejs-lts -y 
	
	Write-Host "Installing Bower"
	choco install bower -y 
	
	Write-Host "Installing MikTex"
	choco install miktex -y
		
	Write-Host "Installing Rufus"
	choco install rufus -y
	
	Write-Host "Installing PuTTY"
	choco install putty -y
	
	
	
}

#Run the appropriate packages according to computer name
Write-Host 'Runing the appropriate packages according to computer name' -Foregroundcolor Yellow -Backgroundcolor Black


If ($env:computername -Like "grff-Win10-Prod*"){

	Write-Host 'This image is being prepared for use on production systems' -Foregroundcolor Yellow -Backgroundcolor Black
	production
	alldesktops

}

ElseIf ($env:computername -Like "grff-Win10-Ops*"){

	Write-Host 'This image is being prepared for use on operations systems' -Foregroundcolor Yellow -Backgroundcolor Black
	operations
	alldesktops
	
}

Else {

	Write-Host 'Have you renamed this VM/System appropriately?' -Foregroundcolor Yellow -Backgroundcolor Black
	
}

	