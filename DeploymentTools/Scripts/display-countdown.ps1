﻿<#
.SYNOPSIS
	This script displays a countdown timer on the screen with a specified
	message from the calling parameters.
	
.DESCRIPTION
	This script displays a countdown timer on the screen with a specified
	message from the calling parameters.  It has a simple text-based display
	as well as a nicer looking popup display style.

	
	The MIT License
	-----------------------------------------------------------------------
	Copyright (c) 2011 Tom Nolan <tom at tinyint dot com>

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the "Software"), 
	to deal in the Software without restriction, including without limitation 
	the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the 
	Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included 
	in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
	DEALINGS IN THE SOFTWARE. 

.NOTES
	File Name  : display-countdown.ps1
	Author     : Tom Nolan - <tom at tinyint dot com>
	Updated    : 2011/08/28
	Requires   : PowerShell V2
	
.EXAMPLE
	.\display-countdown.ps1 -Minutes 15 -Message "Time until break is over"
	
	This will display a simple text-based countdown that will expire 15 minutes
	from the time it was called, displaying the specified message as a header.
	
.EXAMPLE
	.\display-countdown.ps1 -Until (get-date).AddHours(1) -Message "Lunch break" -Popup
	
	This will display a countdown timer that will expire in one hour from the time
	it was called, displayed "Lunch break" as the caption.  This example displays
	in the more user friendly popup mode.

.EXAMPLE
	.\display-countdown.ps1 -Countdown ([TimeSpan]::FromMinutes(10)) -Message "Bio Break" -NoClear
	
	This will display a simple text-based countdown that will expire 10 minutes
	from the time it was called, displaying "Bio Break" as the header.  This call
	does not clear the screen of its contents before running.

.PARAMETER Minutes
Allows the caller to specify the number of minutes to count down.

.PARAMETER Until
Allows the caller to specify a DateTime to count down until.

.PARAMETER Countdown
Allows the caller to specify a TimeSpan value which will be used to count down until.

.PARAMETER Popup
Tells the script to display using the nicer "GUI" style popup.

.PARAMETER NoClear
Do not clear the screen before running.

.LINK
http://tinyint.com/?p=266
#>
Param
(
	[parameter(Mandatory=$true, ParameterSetName="Minutes")]
	[Double]$Minutes,
	[parameter(Mandatory=$true, ParameterSetName="Countdown")]
	[TimeSpan]$Countdown,
	[parameter(Mandatory=$true, ParameterSetName="Until")]
	[DateTime]$Until,
	[parameter(Mandatory=$true)]
	[string]$Message,
	[parameter()]
	[switch]$Popup,
	[parameter()]
	[switch]$NoClear
)

# This is the default timer text view.  It must have three 
New-Variable -Name timeText -Value "{2} seconds" -Option Constant



########################################################################################################
## Do not modify beyond this line

if ($NoClear -ne $true)
{
	Clear-Host
}

# Figure out the target countdown timespan if the function was called with "-minutes" or "-until"
if ($Minutes -ne $null)
{
	$Countdown = [TimeSpan]::FromMinutes($Minutes)
} 
elseif ($Until -ne $null)
{
	$Countdown = $Until.Subtract((Get-Date))
}

# We have to reference "1 second" a few times later so easiest to use a constant
New-Variable -Name oneSec -Value ([TimeSpan]::FromSeconds(1)) -Option Constant

# This is used in the popup display and acts as a minimum character width for the box.  If 
# you change this, make sure you set it as an even number or the window box will display strangely 
# in certain circumstances.
New-Variable -Name minWidth -Value 42 -Option Constant

# We need information about the current host stored for later
$window = $Host.UI.Rawui.WindowSize
$origin = $Host.UI.RawUI.CursorPosition

# Display the simple view if "-Popup" was not used
if ( $Popup -eq $false )
{
    write-host $Message -ForegroundColor Red
    $cursor = $Host.UI.RawUI.CursorPosition
	
    while ($Countdown.TotalSeconds -gt 0)
    {
		# Clear out the old text in case the new one is shorter
		$space = New-Object System.Management.Automation.Host.BufferCell -ArgumentList ' ', $Host.UI.RawUI.ForegroundColor, $Host.UI.RawUI.BackgroundColor, "Complete"
		$rect = New-Object System.Management.Automation.Host.Rectangle -ArgumentList 0, $cursor.Y, $Host.UI.RawUI.WindowSize.Width, $cursor.Y
		$Host.UI.RawUI.SetBufferContents($rect, $space)

		# Display countdown
		$Host.UI.RawUI.CursorPosition = $cursor
		write-host ("System Will Reboot In: $timeText" -f ($Countdown.Days * 24 + $Countdown.Hours).ToString("D1"), $Countdown.Minutes.ToString("D2"), $Countdown.Seconds.ToString("D2"))
        [System.Threading.Thread]::Sleep(1000)
        $Countdown = $Countdown.Subtract($oneSec)
    }
}
else
{
	# Increase the message width by a blank space if the string is an uneven length to make centering the text easier
	if ( $message.Length % 2 -ne 0 ) { $Message += " " }

	# Set the targetWidth of the popup window depending on message size and the maximum time text.
    $targetWidth = $minWidth
	if ( $message.Length + 6 -gt $targetWidth ) { $targetWidth = $Message.Length + 6 }
	$maxTimeWidth = ($timeText -f ($Countdown.Days * 24 + $Countdown.Hours).ToString("D1"), $Countdown.Minutes.ToString("D2"), $Countdown.Seconds.ToString("D2")).Length
	if ( $maxTimeWidth + 6 -gt $targetWidth ) { $targetWidth = $maxTimeWidth + 6 }

	# Finally, increase the target width to an even number if necessary
	if ( $targetWidth % 2 -ne 0 ) { $targetWidth++ }
		
	# Check if the console host is large enough to fit the countdown window within it.  If not exit.
	if ( $window.Width -lt $Message.Length + 6 -or $window.Width -lt $minWidth -or $window.Height -lt 5)
    {
        write-host "This console window is too small for popup display. Enlarge the window and try again." -foregroundcolor red
		break
    }
    else
    {
		# Generate most of the text for the popup window
        $headBars = "─" * (($targetWidth - $Message.Length - 6) / 2)
        $head = "┌─{0} {1} {0}─┐" -f $headBars, $message
		$mid = "│{0}│" -f (" " * ($targetWidth - 2))
		$foot = "└─{0}─┘" -f ("─" * ($targetWidth - 4))

		$capSpace = " " * (($targetWidth - 14 - 2) / 2)
        $caption = "│{0}Rebooting In: {0}│" -f $capSpace

		# Position the mouse cursor for initial output
		$cursor = New-Object System.Management.Automation.Host.Coordinates
		$cursor.x = [int]($window.Width / 2 - $targetWidth / 2)
        $PopupTop = [int]($window.Height / 2 - 3)
		$cursor.y = $PopupTop

		# Write out static text to screen
		$Host.UI.RawUI.CursorPosition = $cursor
        Write-Host -BackgroundColor Red -ForegroundColor White -NoNewLine $head
        $cursor.y += 1
        $Host.UI.RawUI.CursorPosition = $cursor
        Write-Host -BackgroundColor Red -ForegroundColor White -NoNewLine $mid
		$cursor.y += 1
        $Host.UI.RawUI.CursorPosition = $cursor
        Write-Host -BackgroundColor Red -ForegroundColor White -NoNewLine $caption
        $cursor.y += 2
        $Host.UI.RawUI.CursorPosition = $cursor
        Write-Host -BackgroundColor Red -ForegroundColor White -NoNewLine $mid
		$cursor.y += 1
        $Host.UI.RawUI.CursorPosition = $cursor
        Write-Host -BackgroundColor Red -ForegroundColor White -NoNewLine $foot
		
		while ($Countdown.TotalSeconds -ge 0)
        {
			# Generate the time output and add a character to make it even lengthed
			$timeOut = ($timeText -f ($Countdown.Days * 24 + $Countdown.Hours).ToString("D1"), $Countdown.Minutes.ToString("D2"), $Countdown.Seconds.ToString("D2"))
			if ( $timeOut.Length % 2 -ne 0 ) { $timeOut += " " }
			
			# Generate the additional lines of output for the window
			$bodySpace = " " * (($targetWidth - $timeOut.Length - 2) / 2)
            $body = "│{0}{1}{0}│" -f $bodySpace, $timeOut
            
			# Reset cursor position for the time text and write-host
			$cursor.y = $PopupTop + 3
            $Host.UI.RawUI.CursorPosition = $cursor
            Write-Host -BackgroundColor Red -ForegroundColor White -NoNewLine $body

			# Reset the position for the blinking cursor
			$Host.UI.RawUI.CursorPosition = $origin
			[System.Threading.Thread]::Sleep(1000)
            $Countdown = $Countdown.Subtract([System.TimeSpan]::FromSeconds(1))
        }
    }
} 