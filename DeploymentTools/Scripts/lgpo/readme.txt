Documentation Link: https://blogs.technet.microsoft.com/secguide/2016/09/23/lgpo-exe-v2-0-pre-release-support-for-mlgpo-and-reg_qword/

To use this tool: 

1. Create a backup using LGPO.exe /b "<path here> /n <name here>
Example: .\LGPO.exe /b "C:\DeploymentTools\Scripts\lgpo-2" /n 05112017-backup

2. To restore from a backup use: LGPO.exe /g "<full path of backup path here>"
Example: .\LGPO.exe /g "C:\DeploymentTools\Scripts\lgpo-2\{D4E764B1-03E5-449D-BA1E-9E2454DA2B8B}"